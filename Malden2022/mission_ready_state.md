# Mission Ready State

+ Mission Builder - Welcome Mission.
+ Adjust DMS black-area spawning to out side or static spawn zones.
+ Add RHS loot caches, weapons high level flags.
+ Test static spawning AI to see if they conflict. 


# Mission Ideas
+ Permanent vehicles within static spawning areas.