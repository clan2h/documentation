# Malden 2035 in 2022 Welcome Back Bambies

### *New server will be launching shortly for some Arma action.*

## Gaming Systems

__Revive:__ Need a little help to get back on your feet? Much respect to the revivee and reviver. Better than re-spawning cheese. 

__Zombies:__ Love to fuck yourselves some zombies? Wait... fuck-up.... fuck--up some zombies! Then the island is full of them. They tend to be more dominant in the larger towns. This time though I have noticed that they are especially nasty around any type of military locations. Still, the duct tape is worth it.

__@A3XAI:__ Static spawning arseholes. Bunch of mercenaries that currently control the northern section of the island. We don't know where their base is but they are well armed. They have guards posted at key locations across the area. Check your map for ambushes around radio towers, water towers, military structures and petrol stations. As these dudes are controlled by the mod A3XAI they only spawn when the player gets near and will respawn after a set amount of time. They only have a limited amount of respawns.

__Static Loot:__ Looking at your map you will see "Hidden Cache" markers. This indicates there are a number of supply crates within the area. You will need to search for them. The crates have basic supplies but also have a chance of a high level flag. 

__RHS:Russian CustomStaticSpawns:__ Dudes that look like Russian Federation armed forces. These guys are well kitted out and have control of the airport located on the small island of Moray to the far north-west. I'd suggest this a no-fly zone since more than likely they have SAMs. They have also landed on Malden and are currently taking over the southwestern peninsular. There's not much down there but I'd suggest you don't let them advance to far. There are some rumours, Lt Dan rumours, so take them with a grain of salt, that these guys have some serious backing and are loaded with pop-tabs and military tech. These dudes spawn on server start only.

__DMS Missions:__ A bunch of new(ish) missions that will randomly spawn around the map. Just in a day’s work. Got to earn some respect n pop-tabs.
Occupation: Not many random arseholes travelling the wastes these days but there has been reports of several downed choppers. These choppers were carrying supplies and flags. And as we all know, don't we? Flags are the BOSS they can fetch 5, 10 20K from the traders. Unfortunately, nobody knows where these downed choppers are, but sometimes you can see smoke on the horizon that may be well worth an investigation.  

__Car Wreckage Yards:__ Due to the fact that there seems to be so many abandon vehicles just lying about Malden it has attracted several easy located pop-up wreckage yards just waiting for you to make some deliveries.    
Advanced Rope Thingies: Better (probably) rope handling, vehicle towing, sling loading and rappelling from helicopters, rappel down tall buildings, ooooh tingling spider sense.

__Infinity Backpack:__ Room for all the cheese you want. Prices seem to be a bit better on Malden, but the traders require much respect and pop-tabs for one of these bad boys.

__Dual Arms:__ Nothing like having more than one arms. Changes to code may have made this more stable, probably… not.
R3F Logistics: Allows you to handle cargo containers, crates n all the things that hold goodies and move them to and from vehicles. Now you can take back to the suffering civilian population those humanitarian supplies… or just sell them to the traders for profit.

__Traveling Traders:__ Currently the traders that sell the good stuff seem to be driving all over the island and are not always available unless you want to go for a shopping trip.

__Home Comforts 2.0:__ Like always once you have gained enough respect with the traders and locals more comforts will be available. Traders will visit your base, come n stay n eat all your D’Nutz, beans n beer. You can setup a territory within the main trader zones (allows for teleportation). And finally, a bunch of ugly old Chinese washer ladies will come n scrub you n your Reg Grundies.

## XM8 Applications
*In game applications that make your daily life easier. Most cost pop-tabs and respect. Some require a certain respect level before you can use them. The higher the PL-# level (Puss-ay-level: https://cineuropa.org/en/video/215376/rdid/208593/) generally the more it costs.*

__Spawn EMG Vehicle:__ Stuck in the middle of nowhere? Get an instant ride. Not a helicopter FFS. [PL-1]  

__Personal ATM:__ Allows you to make pop-tab transactions no matter where you are.  More realistic than bitcoin. [PL-0]

__Find my Vehicles:__ Left your vehicle somewhere after a BIG night out? Use this and check your map. [PL-2]

__GPS Ping:__ Out looking for salvage, this will mark all vehicles on your map within a 2km radius. [PL-5]

__Mission Spawn:__ Long way from base, by yourself and doing a hard mission? Set a waypoint to spawn on so you can quickly get back into the action. [PL-1] 

__Squad Spawn:__ Group up with your squad by getting teleported straight to them. Job done? Teleport back home to one of your territories. [PL-3] 

__LE Stacks:__ Sick of dying, losing your gear and that orange suit? Let LE clone your loadout, for a price of course. You will be fully restored after death, it is what we do, we thank you for your impatience and pop-tabs. [PL-4]
RDVI PTY LTD: Here at Rust Door Vehicle Insurance, we have your back. Complete crap at driving-n-flying? Don't have enough time to constantly build up your accounts pop-tabs by doing skilful tasks for the community? Well then, we are here to help. For a small flat fee of 1000 pop-tabs and a 20% premium of the purchase price we will fully cover you for when your vehicle is no longer a part of your life. [PL-5]

__Quest Missions:__ Bored of those missions that the enemy just stands there waiting for you to shoot them with your thermal 50x zoomed scoped bi-podded silenced .50 cal sniper rifle from 2.3 Km away? Well, you should be! Activate a mission and squad up, get to know some of the more experienced mercenaries on the island. Cpt Lessly Lissenoop Carnts will have some very interesting and diverse missions to fill in your idle days. Good old George Gunny-7-Toes Watson always seems to have the right gear n ammo for the job and at the right price too. Mike Motorhead Reliant might be only driving on three wheels but he has just what is needed for the mission, whether it be a bunch of ATVs or an attack helicopter. And the man in the field, always there to help just as you head into the thick of things, 2Lt Frederik Leggless Dan. (Note: Dan has two legs and a drinking problem, don’t stare) 

## Costs ##

|App | Respect | Poptabs|
---|---|---|
|EMG Vehicle | 500 | 0 |
|Mission Spawn |250 | 500|
|Squad Spawn| 250 | 2000|
|Flag Spawn | 250 | 1000|
|GPS Ping | 20000 | 0 |
|Find Owned Vehicle | 1000 | 0 |
|LE Stack | 20000 (to activate only) | 10000 per stack |
|Vehicle Insurance | 0 | 1000 flat fee plus 20% of vehicles purchase price |





__*Hope to see you soon!*__