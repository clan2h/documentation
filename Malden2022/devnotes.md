# TODO Malden 2022

# Vanilla Build
- [X] Install SteamCMD
- [X] Install Arma3 Server
- [X] Install DirectX
- [X] MySQL Server : 5.7 
- [X] Disable strict mode : https://www.phpkb.com/kb/article/how-do-i-disable-strict-mode-in-mysql-163.html
- [X] Run db init scripts and setup use
- [X] Update extdb-conf.ini with db and user details
- [X] Vaniala Exile Server : http://exilemod.com/ExileServer-1.0.4a.zip
- [X] Vanilla Steam Client : https://steamcommunity.com/sharedfiles/filedetails/?id=1487484880
- [X] Copy client to server
- [X] Update Key : https://dl.dropboxusercontent.com/s/o2jz1lqeqmfojbe/exile.bikey
- [X] Setup vanilla profile : basic.cfg server.cfg and start.cmd
- [X] Working vanilla server
- [ ] Convert to a start.ps1

# Malden Build

## Server Mods
- [X] Add required Mods (required mods are anything I've hard coded into the server/client) 
- [X] CBA_A3
- [X] CUPs Weapons, Units, Vehicles
- [X] RHSUSAF, RHSAFRF
- [X] Test Load Vanilla Server + Client

## MySQL
- [X] Patch database with Clan2h updates from clan2h_server\notes\exile.ini, may have to rebuild a update.sql

## Mission File
- [X] Patch for Mission respawn ???

## Build Malden 2022.3 Server @ExileServer.Malden
- [X] Addon: exile_server_config.pbo update for Malden and 2h functions / mods
- [X] Addon: clan2h_server.pbo 
- [X] Addon: clan2h_mission_builder.pbo
- [X] clan2h_mission_builder configured and mission folder injected
- [X] Build custom PS script to build PBOs from repositories then copy to server
- [X] Add back admin mode (zues)
- [X] Stacks: add help text to XM8 and display costs. Server side cost checks plus client side check to disable buttons. COMMIT:: Stack 2.0 on BRANCH:: Malden2022
- [W] Patch: description.ext for debugging console

## Other Client Mods
 - [X] DualArms - https://forums.bohemia.net/forums/topic/231616-exile-install-dualarms-two-primary-weapons/
 - [X] Infinity backpack
 - [X] Revive script of some type - https://github.com/hpy/Enigma_Exile_Revive - Enigma needs my patches for the mission system 
 - [X] New towing script - https://github.com/neverwinter-nights/Vehicle-Towing-Script - 
 - [X] RHF Towing / R3F Logistics :: https://github.com/GamingAtDeathsDoor/R3F || https://github.com/stcrowe/R3FAdvLog || https://github.com/slb2k11/Exile_R3F
 - [X] Ropes - https://steamcommunity.com/workshop/filedetails/?id=643080051 - All ropes = https://github.com/sethduda || had to use @AdvancedRappelling from steam as errors
 - [X] harvest drugs and mining ores ???
 - [X] Travelling trader https://github.com/secondcoming/ExileTravellingTrader/tree/master/source/a3_travellingTrader
 - [?] https://github.com/DevZupa/ZCP-A3-Exile
 - [X] Trader resources https://github.com/redned70/Trader-Mod || https://github.com/aussie-battler/Travelling-Trader
 - [X] https://github.com/Andrew-S90/ExileRevive
 - [I] Lots here https://github.com/MrGoodTrust/arma3exile/tree/main/Arma%203%20Exile%20Addons%20and%20Scripts
 - [?] https://github.com/bigfootcode/bigfoots-shipwrecks
 - [X] https://github.com/BdMdesigN/Igiload-Reloaded
 - [X] https://github.com/GamingAtDeathsDoor/Exile-Status-Bar
 - [X] https://github.com/hpy/Enigma_Exile_Revive
 - [X] https://github.com/magn456/exile_loadout
 - [X] https://github.com/Ghostrider-DbD-/Rearm-Repair-Refuel- 
 - [X] https://github.com/aussie-battler/Exile-Plants
 - [X] Advanced-Vehicle-Service-Centre-master
 - [X] Pauls 29 Missions for DMS

## @ExileServer.Malden gameplay features
- [X] Addon: a3_dms.pbo
- [X] Addon: a3_exile_occupation.pbo
- [X] Addon: exilz_mop.pbo (Triggers configured for Malden) :: (https://github.com/Sir-Joker/ExileZ-Mod/blob/master/GetMarkerCmd.txt)
- [X] Update DB for Zombies
- [X] Addon: ryanzombies.pbo 
- [X] Configure Mission SQF for zombies - addOns[]= { "exile_client", "a3_map_altis", "Ryanzombies", "ryanzombiesfunctions" // Make sure the last entry does not have a comma after it!! };
- [?] Add back some DMS_PersistentVehicle :: A grave yard = Suprise find maybe
- [X] Adjust dynamic car spawn (and types) :: Change grid to 750
- [W] Add a check and config var that charges the player (account) for spawning back with a load out. (use respect - $)
- [X] Need to remove dead body, but how? See Overrides\PlayerEvents\ExileServer_object_player_event_onMpKilled.sqf
- [X] Add gameplay mechanic for infinity backpack - PRICE + RESPECT will do for now
- [X] Add a couple of good waste dumps out in the open 
- [ ] NEW APP:: Car insurance - XM8 - If in gararge, can insure, when destroyed put back in gararge take money n bit of respect (number of claims :) history )
- [X] A3XAI Config:: Launcher Types :: Static AI
- [X] DMS Config (black zones) - 
    	+ [[1660,1980],2000], // Bottom left malden  
		+ [[10656,3725],2000], // East Island  
		+ [[1000,12133],1000] // North island
- [X] Occupation Config ( no black zones), added stolenflags to down choppas
- [X] https://github.com/zen-mod/ZEN
- [W] Add Debugging traders in my territory.
- [W] Add Debugging Code to ally me to teleport with out a flag check
- [W] Add a flag at the traders? (Disabled Checks)

## Malden Client
- [X] mission.sqf missing respawn with loadout?
- [X] mission.sqf missing death spawn? LEAVE OUT FROM NOW!!!
- [X] ExileServer_object_player_createBambi is missing last code to support the loadout/respawn functions, this I moved it to here ExileServer_object_player_database_load
- [X] Configure config.cpp for Malden
- [X] is A3XAI_Client needed? No it's OPTIONAL look to be only for radio.

## Mission Builder
- [W] Design a welcome to Malden mission template
- [X] Where is my fast travel code? - Found it - Need to progress the stage first.
- [?] Code to load zombie markers into mission editor ?
- [?] Code to build A3XAI static spawn locations

### New Features
- [ ] CLEANUP:: missionbuildereditor.stratis : Feature F005_Zombies
- [X] Insurance APP.
- [ ] Spotlight - this I will need to learn how to dynamic program and attach to areas/spawn AI in my mission builder 
- [ ] AI stuff ... https://forums.bohemia.net/forums/topic/225402-lambs-improved-dangerfsm/ thsi can jsut be loaded into server then on clients
- [ ]  -Scripts to spawn in static loot, Objects (contrtuctions) and static vehicles like AA 

## XM8 Applications
- [X] Spawn EMG Vehicle: Stuck in the middle of knowhere? Get a instant ride.    
- [X] Personal ATM: Allows you to make poptab transactions no matter where you are. 
- [X] Find my Vehicles: Left your vehicle somewhere after a BIG night. Use this and check your map.
- [X] GPS Ping: Out looking for salvage, this will mark all vehicles on your map within a 2km radius.
- [X] Mission Spawn: Long way from base, by yourself and doing a hard mission, Set a waypoint to spawn on so you can quickly get back into the action. 
- [X] Squad Spawn: Group up with your squad by getting teleported straight to them. Job done? Teleport back home to one of your territories. 
- [X] LE Stacks: Sick of dying, losing your gear and that orange suit? Let LE clone your loadout, for a price of course. You will be fully restored after death, it is what we do, we thank you for your impatience and poptabs.
- [X] RDVI PTY LTD: Here at Rust Door Vehicle Insurance we have your back. Complete crap at driving-n-flying? Don't have enough time to constantly build up your accounts poptabs by doing skillful tasks for the community? Well then, we are here to help. For a small flat fee of 1000pt and a 20% premium of the purchase price we will fully cover you for when your vehicle is no longer a part of your life.   

### Current Issues
- [x] IOSSUE:: No loss of $$$ when transporting - yes IT WAS have to have the cash on you.
- [X] LOGIC:: Revive and R3F loads twice on first connect? Is it only the first player or what? 
- [X] ISSUE:: Traders missing classes for guns n ammo (see below)
- [X] FIX:  end user goes into arsenal and loads gun then adds all ammo and export, send to me to update files.
- [X] ISSUE:: CUPS and RHS items have a 0 purchace price?
- [X] ISSUE:: RFH guns (or CUPS) not showing number of mags :: entry point for this
- [?] CODE:: Why is revive client loading twice?
- [X] CODE:: Make sure no money in stacks
- [X] CODE:: Need a function to create a default stack entry on account create.
- [X] ERROR:: XM8 Find My Car
- [X] ERROR:: Stack functions not working
- [X] ERROR:: Setting spawn point and not a family member causes issues - Ok for now.
- [X] ERROR:: 17:17:21 File clan2h_server\exile\ExileServer_Clan2h_network_FindVehicle.sqf..., line 48
- [X] ERROR:: DMS:: 20:28:44 File x\addons\dms\scripts\fn_MissionParams.sqf..., line 135 [FIX::https://github.com/Defent/DMS_Exile/tree/master/%40ExileServer/addons/a3_dms/map_configs]